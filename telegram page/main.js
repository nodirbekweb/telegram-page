$(document).ready(function () {
    var contacts = [];
    var activeContact = null;

    $("#addContact").click(function () {
        var userName = $("#userName").val();
        var tel = $("#tel").val();
        if (userName !== "" && tel !== ""){
            if(contacts.length === 0){
                contacts.push({
                    name: userName,
                    telnum: tel,
                    chat: []
                });
            }
            else{
                var status = false;
                contacts.forEach((item)=>{
                    if(item.telnum === tel){
                        status = true;
                    }
                });
                if(status){
                    alert("Bunday kontakt bor!")
                }else {
                    contacts.push({
                        name: userName,
                        telnum: tel,
                        chat: []
                    })
                }
            }

            $("#contactModal").modal("hide");
            drawCntact();
            $("#userName").val("");
            $("#tel").val("");
        }
    });
    function drawCntact() {
        var div = "";
        contacts.forEach((item)=>{
            div += "<div class='px-4 py-2 border-bottom contact-item' data-id='"+item.telnum+"'>" +
                "<h4 class='mb-2 text-white font-weight-normal'>"+item.name+"</h4>" +
                "<p class='small mb-0 text-light'>"+item.telnum+"</p>" +
                "</div>"
        });
        $(".contacts").html(div);
        $(".contact-item").click(function () {
            var id = $(this).attr("data-id");
            contacts.forEach(item=>{
                if (item.telnum == id){
                    activeContact = item;
                }
            });
            $("#contactName").text(activeContact.name);
            $("#contactTel").text(activeContact.telnum);
            $("#messageBlock").removeClass("d-none");
            drawMessage();
        });
    }
    function drawMessage(){
        var div = "";
        activeContact.chat.forEach(item=>{
            div +=' <div class="ml-auto px-3 mb-2 py-2 small bg-warning">\n' +
                item.message +
                '                </div>'
        });
        $(".message-block").html(div);
    }
    $("#addMessage").click(function () {
        var id = activeContact.telnum;
        var text = $("#message").val();
        for(var i = 0; i < contacts.length; i++){
            if (contacts[i].telnum === id){
                contacts[i].chat.push({
                    message: text
                });
                activeContact = contacts[i];
            }
        }
        drawMessage();
    });
});